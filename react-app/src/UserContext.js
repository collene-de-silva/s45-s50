import React from "react";

/* 
    2 Keywords: 
        1. Provider - one that makes something global 
        2. Consumer - page that will be using that global state
*/

const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use (making global)
//the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;