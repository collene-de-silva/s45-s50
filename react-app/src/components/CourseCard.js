import { useState, useEffect } from "react";
import { Button, Card} from "react-bootstrap";
import { Link } from "react-router-dom";


export default function CourseCard({courseProp}) {

    // console.log(props)
    // console.log(typeof props)
    // console.log(courseProp)
    const { name, description, price, _id } = courseProp
    // console.log(courseProp)

    /* 
        Syntax:
            const [getter, setter] = useState(initialGetterValue)
    */
//    const [count, setCount] = useState(0)
//    const [seats, setSeat] = useState(30)    

//    function enroll() {
//         setCount(count + 1)                 //asynchronous
//         setSeat(seats - 1)                   //asynchronous
//        console.log('Enrollees:' + count)
//        console.log('Seat:' + seats)    
//    }

//    useEffect(() => {
//        if (seats === 0) {
//            alert('No more seats available!')
//        }
//    }, [seats])
   /*  
        Syntax: 
            useEffect(() => {
                //function to run
            }, optional array)

            optional array - dinadagdag natin to para kapag walang changes sa optional array, hindi magrurun ang function ng useEffect

            if there are no optional array - mag rurun lagi ang useEffect every update/render ng page
   */

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>

                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>
                {description}
                </Card.Text>

                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>
                PhP {price}
                </Card.Text>

                {/* <Card.Text>
                Enrollees: {count}
                </Card.Text>

                <Card.Text>
                Seat: {seats}
                </Card.Text>


                <Button variant="primary" onClick={enroll}>Enroll</Button> */}

                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}